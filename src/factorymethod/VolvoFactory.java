/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author aluno.redes
 */
public class VolvoFactory implements FactoryCar {

    @Override
    public Car makeCar(CarModels model) {
        switch (model) {
            case XC40:
                return new XC40();
            case XC60:
                return new XC60();
            case XC90:
                return new XC90();
                
            default:
                return null;
        }
    }

}
