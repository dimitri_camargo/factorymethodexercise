/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author aluno.redes
 */
public class ToyotaFactory implements FactoryCar{

    @Override
    public Car makeCar(CarModels model) {
        switch (model) {
            case corolla:
                return new Corolla();
            default:
                return null;
        }
    }
    
}
