/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author aluno.redes
 */
public enum CarModels {
    uno, //equivale ao numero 0
    gol, //equivale ao numero 1
    corolla, //equivale ao numero 2
    XC40,
    XC60,
    XC90
}
